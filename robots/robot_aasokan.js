links_geom_imported = false;

//////////////////////////////////////////////////
/////     DEFINE ROBOT AND LINKS
//////////////////////////////////////////////////

// create robot data object
robot = new Object(); // or just {} will create new object

// give the robot a name
robot.name = "rover";

var wheel_radius=0.15
// initialize start pose of robot in the world
robot.origin = {xyz: [0,wheel_radius,0], rpy:[0,0,0]};  

// specify base link of the robot; robot.origin is transform of world to the robot base
robot.base = "base";  

// specify and create data objects for the links of the robot
robot.links = {
    "base": {},  
    "front_left_wheel": {}, 
    "front_right_wheel": {}, 
    "rear_left_wheel": {}, 
    "rear_right_wheel": {}, 
    "front_left_axle": {}, 
    "front_right_axle": {}, 
    "rear_left_axle": {}, 
    "rear_right_axle": {}, 
    "arm_base": {},
    "arm_shoulder": {},
    "arm_elbow":{},
    "arm_palm":{}
    //"clavicle_left": {} , 
    //"shoulder_right": {}, 
    //"upperarm_right": {}, 
    //"forearm_right": {} 
};
/* for you to do
, "shoulder_left": {}  , "upperarm_left": {} , "forearm_left": {} };
*/

//////////////////////////////////////////////////
/////     DEFINE JOINTS AND KINEMATIC HIERARCHY
//////////////////////////////////////////////////

/*      joint definition template
        // specify parent/inboard link and child/outboard link
        robot.joints.joint1 = {parent:"link1", child:"link2"};
        // joint origin's offset transform from parent link origin
        robot.joints.joint1.origin = {xyz: [5,3,0], rpy:[0,0,0]}; 
        // joint rotation axis
        robot.joints.joint1.axis = [0.0,0.0,1.0]; 
*/


// roll-pitch-yaw defined by ROS as corresponding to x-y-z 
//http://wiki.ros.org/urdf/Tutorials/Create%20your%20own%20urdf%20file

// specify and create data objects for the joints of the robot
robot.joints = {};

robot.joints.front_left_fixed = {parent:"front_left_axle", child:"front_left_wheel"};
robot.joints.front_left_fixed.origin = {xyz: [0,0.2,0], rpy:[0,0,0]};
robot.joints.front_left_fixed.axis = [0.0,0.1,0.0]; 

robot.joints.front_right_fixed = {parent:"front_right_axle", child:"front_right_wheel"};
robot.joints.front_right_fixed.origin = {xyz: [0,-0.2,0], rpy:[0,0,0]};
robot.joints.front_right_fixed.axis = [0.0,-0.1,0]; 

robot.joints.rear_left_fixed = {parent:"rear_left_axle", child:"rear_left_wheel"};
robot.joints.rear_left_fixed.origin = {xyz: [0,0.2,0], rpy:[0,0,0]};
robot.joints.rear_left_fixed.axis = [0.0,0.1,0]; 

robot.joints.rear_right_fixed = {parent:"rear_right_axle", child:"rear_right_wheel"};
robot.joints.rear_right_fixed.origin = {xyz: [0,-0.2,0], rpy:[0,0,0]};
robot.joints.rear_right_fixed.axis = [0.0,-0.1,0]; 

robot.joints.front_left_roll = {parent:"base", child:"front_left_axle"};
robot.joints.front_left_roll.origin = {xyz: [0.35,0.2,0.5], rpy:[0,Math.PI,-Math.PI/2]};
robot.joints.front_left_roll.axis = [0.0,0.1,0.0]; 

robot.joints.front_right_roll = {parent:"base", child:"front_right_axle"};
robot.joints.front_right_roll.origin = {xyz: [-0.35,0.2,0.5], rpy:[0,Math.PI,-Math.PI/2]};
robot.joints.front_right_roll.axis = [0.0,-0.1,0]; 

robot.joints.rear_left_roll = {parent:"base", child:"rear_left_axle"};
robot.joints.rear_left_roll.origin = {xyz: [0.35,0.2,-0.5], rpy:[0,Math.PI,-Math.PI/2]};
robot.joints.rear_left_roll.axis = [0.0,0.1,0]; 

robot.joints.rear_right_roll = {parent:"base", child:"rear_right_axle"};
robot.joints.rear_right_roll.origin = {xyz: [-0.35,0.2,-0.5], rpy:[0,Math.PI,-Math.PI/2]};
robot.joints.rear_right_roll.axis = [0.0,-0.1,0]; 

robot.joints.arm_base_yaw = {parent:"base", child:"arm_base"};
robot.joints.arm_base_yaw.origin = {xyz: [0,0.5,0], rpy:[0,Math.PI,-Math.PI/2]};
robot.joints.arm_base_yaw.axis = [0.1,0,0]; 

robot.joints.arm_shoulder_pitch = {parent:"arm_base", child:"arm_shoulder"};
robot.joints.arm_shoulder_pitch.origin = {xyz: [0.6,0,0], rpy:[0,Math.PI,-Math.PI/2]};
robot.joints.arm_shoulder_pitch.axis = [0,0,0.1]; 

robot.joints.arm_elbow_pitch = {parent:"arm_shoulder", child:"arm_elbow"};
robot.joints.arm_elbow_pitch.origin = {xyz: [0,0.85,-0.175], rpy:[Math.PI/2,0,0]};
robot.joints.arm_elbow_pitch.axis = [0,0,0.1]; 

robot.joints.arm_palm_yaw = {parent:"arm_elbow", child:"arm_palm"};
robot.joints.arm_palm_yaw.origin = {xyz: [0,0.85,0], rpy:[-Math.PI/2,0,0]};
robot.joints.arm_palm_yaw.axis = [0,0,0.1]; 

/*
robot.joints.shoulder_right_yaw = {parent:"clavicle_right", child:"shoulder_right"};
robot.joints.shoulder_right_yaw.origin = {xyz: [0.0,-0.15,0.85], rpy:[Math.PI/2,0,0]};
robot.joints.shoulder_right_yaw.axis = [0.0,0.707,0.707]; 

robot.joints.upperarm_right_pitch = {parent:"shoulder_right", child:"upperarm_right"};
robot.joints.upperarm_right_pitch.origin = {xyz: [0.0,0.0,0.7], rpy:[0,0,0]};
robot.joints.upperarm_right_pitch.axis = [0.0,1.0,0.0]; 

robot.joints.forearm_right_yaw = {parent:"upperarm_right", child:"forearm_right"};
robot.joints.forearm_right_yaw.origin = {xyz: [0.0,0.0,0.7], rpy:[0,0,0]};
robot.joints.forearm_right_yaw.axis = [1.0,0.0,0.0]; 

robot.joints.clavicle_left_roll = {parent:"base", child:"clavicle_left"};
robot.joints.clavicle_left_roll.origin = {xyz: [-0.3,0.4,0.0], rpy:[-Math.PI/2,0,0]};
robot.joints.clavicle_left_roll.axis = [0.0,0.0,1.0]; 
*/

// specify name of endeffector frame
robot.endeffector = {};
robot.endeffector.frame = "front_left_roll";
robot.endeffector.position = [[0],[0],[0.5],[1]];

//////////////////////////////////////////////////
/////     DEFINE LINK threejs GEOMETRIES
//////////////////////////////////////////////////

/*  threejs geometry definition template, will be used by THREE.Mesh() to create threejs object
    // create threejs geometry and insert into links_geom data object
    links_geom["link1"] = new THREE.CubeGeometry( 5+2, 2, 2 );

    // example of translating geometry (in object space)
    links_geom["link1"].applyMatrix( new THREE.Matrix4().makeTranslation(5/2, 0, 0) );

    // example of rotating geometry 45 degrees about y-axis (in object space)
    var temp3axis = new THREE.Vector3(0,1,0);
    links_geom["link1"].rotateOnAxis(temp3axis,Math.PI/4);
*/

// define threejs geometries and associate with robot links 
links_geom = {};

links_geom["base"] = new THREE.CubeGeometry( 1, 0.3, 1.5 );
links_geom["base"].applyMatrix( new THREE.Matrix4().makeTranslation(0, 0.2, 0) );
/*
var wheelAssembly = new THREE.Geometry();
var wheel=new THREE.CylinderGeometry( wheel_radius*2, wheel_radius*2, 0.15);
var wheel_axle=new THREE.CylinderGeometry( wheel_radius*0.1, wheel_radius*0.1, 0.2);
var wheelMesh=new THREE.Mesh(wheel);
var wheel_axleMesh=new THREE.Mesh(wheel_axle);
wheelMesh.updateMatrix(); // as needed
wheel_axleMesh.updateMatrix();
wheelAssembly.merge(wheelMesh.geometry, wheelMesh.matrix);
wheelAssembly.merge(wheel_axleMesh.geometry, wheel_axleMesh.matrix);

var material = new THREE.MeshPhongMaterial({color: 0xFF0000});
var mesh = new THREE.Mesh(wheelAssembly, material);
*/
links_geom["front_left_wheel"] = new THREE.CylinderGeometry( wheel_radius*2, wheel_radius*2, 0.15)
links_geom["front_left_wheel"].applyMatrix( new THREE.Matrix4().makeTranslation(0, 0.15, 0) );

links_geom["front_right_wheel"] = new THREE.CylinderGeometry( wheel_radius*2, wheel_radius*2, 0.15);
links_geom["front_right_wheel"].applyMatrix( new THREE.Matrix4().makeTranslation(0, -0.15, 0) );

links_geom["rear_left_wheel"] = new THREE.CylinderGeometry( wheel_radius*2, wheel_radius*2, 0.15);
links_geom["rear_left_wheel"].applyMatrix( new THREE.Matrix4().makeTranslation(0, 0.15, 0) );

links_geom["rear_right_wheel"] = new THREE.CylinderGeometry( wheel_radius*2, wheel_radius*2, 0.15);
links_geom["rear_right_wheel"].applyMatrix( new THREE.Matrix4().makeTranslation(0, -0.15, 0) );

links_geom["front_left_axle"] = new THREE.CylinderGeometry( wheel_radius*0.5, wheel_radius*0.5, 0.15)
links_geom["front_left_axle"].applyMatrix( new THREE.Matrix4().makeTranslation(0, 0.15, 0) );

links_geom["front_right_axle"] = new THREE.CylinderGeometry( wheel_radius*0.5, wheel_radius*0.5, 0.15);
links_geom["front_right_axle"].applyMatrix( new THREE.Matrix4().makeTranslation(0, -0.2, 0) );

links_geom["rear_left_axle"] = new THREE.CylinderGeometry( wheel_radius*0.5, wheel_radius*0.5, 0.15);
links_geom["rear_left_axle"].applyMatrix( new THREE.Matrix4().makeTranslation(0, 0.2, 0) );

links_geom["rear_right_axle"] = new THREE.CylinderGeometry( wheel_radius*0.5, wheel_radius*0.5, 0.15);
links_geom["rear_right_axle"].applyMatrix( new THREE.Matrix4().makeTranslation(0, -0.2, 0) );

links_geom["arm_base"] = new THREE.CubeGeometry( 1, 0.2, 0.2 );
links_geom["arm_base"].applyMatrix( new THREE.Matrix4().makeTranslation(0.2, 0, 0));

links_geom["arm_shoulder"] = new THREE.CubeGeometry( 0.15, 0.7, 0.15 );
links_geom["arm_shoulder"].applyMatrix( new THREE.Matrix4().makeTranslation(0, 0.45, 0));

links_geom["arm_elbow"] = new THREE.CubeGeometry( 0.15, 0.7, 0.15 );
links_geom["arm_elbow"].applyMatrix( new THREE.Matrix4().makeTranslation(0, 0.45, 0));

links_geom["arm_palm"] = new THREE.CubeGeometry( 0.5, 0.15, 0.1 );
links_geom["arm_palm"].applyMatrix( new THREE.Matrix4().makeTranslation(0, 0, 0));

