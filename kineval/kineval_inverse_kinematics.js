
/*-- |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/|

    KinEval | Kinematic Evaluator | inverse kinematics

    Implementation of robot kinematics, control, decision making, and dynamics 
        in HTML5/JavaScript and threejs
     
    @author ohseejay / https://github.com/ohseejay / https://bitbucket.org/ohseejay

    Chad Jenkins
    Laboratory for Perception RObotics and Grounded REasoning Systems
    University of Michigan

    License: Creative Commons 3.0 BY-SA

|\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| --*/

kineval.robotInverseKinematics = function robot_inverse_kinematics(endeffector_target_world, endeffector_joint, endeffector_position_local) {

    // compute joint angle controls to move location on specified link to Cartesian location
    if ((kineval.params.update_ik)||(kineval.params.persist_ik)) { 
        // if update requested, call ik iterator and show endeffector and target
        kineval.iterateIK(endeffector_target_world, endeffector_joint, endeffector_position_local);
        if (kineval.params.trial_ik_random.execute)
            kineval.randomizeIKtrial();
        else // KE: this use of start time assumes IK is invoked before trial
            kineval.params.trial_ik_random.start = new Date();
    }

    kineval.params.update_ik = false; // clear IK request for next iteration
}

kineval.randomizeIKtrial = function randomIKtrial () {

    // update time from start of trial
  cur_time = new Date();
  kineval.params.trial_ik_random.time = cur_time.getTime()-kineval.params.trial_ik_random.start.getTime();
  // get endeffector Cartesian position in the world
  endeffector_world = matrix_multiply(robot.joints[robot.endeffector.frame].xform,robot.endeffector.position);
  // compute distance of endeffector to target
  kineval.params.trial_ik_random.distance_current = Math.sqrt(
          Math.pow(kineval.params.ik_target.position[0][0]-endeffector_world[0][0],2.0)
          + Math.pow(kineval.params.ik_target.position[1][0]-endeffector_world[1][0],2.0)
          + Math.pow(kineval.params.ik_target.position[2][0]-endeffector_world[2][0],2.0) );
  // if target reached, increment scoring and generate new target location
  // KE 2 : convert hardcoded constants into proper parameters
  if (kineval.params.trial_ik_random.distance_current < 0.01) {
      kineval.params.ik_target.position[0][0] = 1.2*(Math.random()-0.5);
      kineval.params.ik_target.position[1][0] = 1.2*(Math.random()-0.5)+1.5;
      kineval.params.ik_target.position[2][0] = 0.7*(Math.random()-0.5)+0.5;
      kineval.params.trial_ik_random.targets += 1;
      textbar.innerHTML = "IK trial Random: target " + kineval.params.trial_ik_random.targets + " reached at time " + kineval.params.trial_ik_random.time;
  }
}

kineval.iterateIK = function iterate_inverse_kinematics(endeffector_target_world, endeffector_joint, endeffector_position_local) {

    // STENCIL: implement inverse kinematics iteration
    var joint_xform = robot.joints[endeffector_joint].xform;
    var endeffector_position_world = matrix_multiply(joint_xform, endeffector_position_local);

    //console.log("Target: " +endeffector_target_world.position);
    //console.log("Pos: " +endeffector_position_world);
    var errorTrans = vector_addsub(endeffector_target_world.position, endeffector_position_world,0); 
    error = [errorTrans[0],errorTrans[1],errorTrans[2],0,0,0];
    if(kineval.params.ik_orientation_included){
        var angles = eulerAngles(joint_xform);
        var errorRot = vector_addsub(endeffector_target_world.orientation, angles,0);
        error = [errorTrans[0],errorTrans[1],errorTrans[2],errorRot[0],errorRot[1],errorRot[2]];
    }
    
    //console.log(error);
    computeJacobian(endeffector_joint, endeffector_target_world.position, error);
    //console.log("Jacobian size "+J.length+","+J[0].length);
    //console.log("error size "+error.length+","+error[0].length);
    //var Jinv;
    //console.log("1");
    
    //console.log("Inv size "+Jinv.length+","+Jinv[0].length);
    //console.log("Jinv", Jinv);
    //console.log("3");
    //var q=matrix_multiply(J,error);
    //prompt();
    //console.log("q");
    //console.log(q);
    
    //var success = applyVelocity(q, endeffector_joint);
    //console.log(success);
    
}

function computeSubJacobian(joint, endeffector_target_world){
    var j;
    //var parent_joint=robot.joints[joint.parent];
    //var axis=matrix_multiply(parent_joint.axis,parent_joint.xform[3]);
    //prompt();
    var axisRaw=matrix_multiply(joint.xform,[[joint.axis[0]],[joint.axis[1]],[joint.axis[2]],[0]]);
    //console.log([[joint.axis[0]],[joint.axis[1]],[joint.axis[2]],[1]]);
    //console.log(joint.xform)
    //prompt();
    //console.log(axisRaw);
    axis=vector_normalize([axisRaw[0][0],axisRaw[1][0],axisRaw[2][0]]);
    //console.log("Axis: "+ axis);
    if(joint.type=='prismatic'){
        j=[axis[0],axis[1],axis[2],0,0,0];
    }
    else{  // Default behaviour for other joints, including revolute and continuous

        var xform = [joint.xform[0][3],joint.xform[1][3],joint.xform[2][3], joint.xform[3][3]];
        //console.log("xform: "+xform);
        //console.log(joint.xform);
        var diff2D = vector_addsub(endeffector_target_world,xform,0);
        var diff=[diff2D[0],diff2D[1],diff2D[2]];
        var j123=vector_cross(axis,diff);

        //console.log("J123: "+j123);
        //console.log(axis[0][0]);
        
        j=[j123[0],j123[1],j123[2],axis[0],axis[1],axis[2]];
    }
    
    return j;
}
    
function computeJacobian(endeffector_joint, endeffector_target_world, error){
    var curr_joint,i=0;
    var step=kineval.params.ik_steplength;
    curr_joint=robot.joints[endeffector_joint];
    var j;
    var Jt=[];
    while(1){
        
        if(curr_joint.type!='fixed'){
            
            j=computeSubJacobian(curr_joint, endeffector_target_world);
            Jt[i]=j;
            i++;
            var q=vector_dot(j,error);
            //console.log(q);
            curr_joint.control=step*q;
            
        }
        //do {
        if(!robot.links[curr_joint.parent].hasOwnProperty("parent")){
                break;
        }
        else{
            var parent_link = robot.links[curr_joint.parent];
            //console.log(parent_link);
            curr_joint = robot.joints[parent_link.parent];
        }
        //} while(curr_joint.type === 'fixed');
        //console.log(curr_joint.type === 'fixed');
        
        //console.log("Parent");
        //console.log(curr_joint);
        //console.log(robot.links[curr_joint.parent]);
    }

    if(kineval.params.ik_pseudoinverse){
        Jt = (matrix_pseudoinverse(matrix_transpose(Jt)));
    }
    //console.log(Jt);
   // applyVelocity(Jt,endeffector_joint);
    

/*
    //FOR LAST JOINT
    j=computeSubJacobian(curr_joint, endeffector_target_world);

    if(kineval.params.ik_pseudoinverse){
        //console.log("2");
        j = matrix_pseudoinverse(j);
    }
    //console.log("j "+j);
    //console.log("error "+error);
    var q=vector_dot(j,error);
    //console.log(q);
    curr_joint.control=step*q;
    //console.log(curr_joint);
    //console.log(J);
    */
}

function eulerAngles(xform){

    var a,b,g;
    console.log((-1)*xform[0][1]/xform[0][0]);
    g = Math.atan((-1)*xform[0][1]/xform[0][0]);
    b = Math.asin(xform[0][2]);
    a = Math.acos(xform[2][2]/Math.cos(b));

    return [a,b,g];
}


function applyVelocity(J, endeffector_joint){
    var step=kineval.params.ik_steplength;
    var curr_joint=robot.joints[endeffector_joint];
    var i=0;

    while(1){
        
        if(curr_joint.type!='fixed'){
            
            var j=J[i];

            var q=vector_dot(j,error);
            //console.log(q);
            curr_joint.control=step*q;
            i++;
        }
        //do {
        if(!robot.links[curr_joint.parent].hasOwnProperty("parent")){
                break;
        }
        else{
            var parent_link = robot.links[curr_joint.parent];
            //console.log(parent_link);
            curr_joint = robot.joints[parent_link.parent];
        }
        //} while(curr_joint.type === 'fixed');
        //console.log(curr_joint.type === 'fixed');
        
        //console.log("Parent");
        //console.log(curr_joint);
        //console.log(robot.links[curr_joint.parent]);
    }
}


