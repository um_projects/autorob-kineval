
/*-- |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/|

    KinEval | Kinematic Evaluator | forward kinematics

    Implementation of robot kinematics, control, decision making, and dynamics 
        in HTML5/JavaScript and threejs
     
    @author ohseejay / https://github.com/ohseejay / https://bitbucket.org/ohseejay

    Chad Jenkins
    Laboratory for Perception RObotics and Grounded REasoning Systems
    University of Michigan

    License: Creative Commons 3.0 BY-SA

|\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| --*/

kineval.robotForwardKinematics = function robotForwardKinematics () { 

    kineval.buildFKTransforms=buildFKTransforms;

    if (typeof kineval.buildFKTransforms === 'undefined') {
        textbar.innerHTML = "forward kinematics not implemented";
        return;
    }
    else{     
        buildFKTransforms(robot,generate_identity(4));

        //Coordinate correction for ROS imported geometries
        if (robot.links_geom_imported){
            var rotMat = specialRotate(0,-Math.PI/2,-Math.PI/2);
            robot.xform=matrix_multiply(robot.xform,rotMat);
        }
        
        traverseFKBase(robot.links[robot.base],robot.xform);
    }

    // STENCIL: implement kineval.buildFKTransforms();
    
}
function buildFKTransforms(element,xform){
    var xyz,rpy,T,Rx,Ry,Rz,trans,trans1,trans2;
    
    xyz=element.origin.xyz;
    rpy=element.origin.rpy;
    //console.log("Processing now");
    //console.log(element.name);
    T=generate_translation_matrix(xyz[0],xyz[1],xyz[2]);
    //console.log(T)
    
    Rx=generate_rotation_matrix_X(rpy[0]);
    //console.log(Rx)
    Ry=generate_rotation_matrix_Y(rpy[1]);
    //console.log(Ry)
    Rz=generate_rotation_matrix_Z(rpy[2]);
    //console.log(Rz)
    //prompt();
    
    trans=matrix_multiply(Ry,Rx);
    trans1=matrix_multiply(Rz,trans);

    if(typeof element.type!='undefined'){
        if(element.type==='prismatic'){
        //var q=quaternion_from_axisangle(element.axis,0);
            var T1=generate_translation_matrix(0,0,element.angle);
            trans1=matrix_multiply(T1,trans1);
        }
        else{
            var q=quaternion_from_axisangle(element.axis,element.angle);
                //q=quaternion_normalize(q);
            var rotation=quaternion_to_rotation_matrix(q);
            trans1=matrix_multiply(trans1,rotation);
                //element.xform=matrix_multiply(xform,rotation);
        }
    }
    else{
        if(typeof element.axis != 'undefined'){
            var q=quaternion_from_axisangle(element.axis,element.angle);
                //q=quaternion_normalize(q);
            var rotation=quaternion_to_rotation_matrix(q);
            trans1=matrix_multiply(trans1,rotation);
        }
            //element.xform=matrix_multiply(xform,rotation);
    }
    trans2=matrix_multiply(T,trans1);
    
    
    element.xform=matrix_multiply(xform,trans2);
    

    //console.log(RyRx);
    //transform=matrix_multiply(T,R);
    //console.log(transform);
    
    //element.xform=trans3
    //console.log(element.xform);
    
}
 
    // STENCIL: reference code alternates recursive traversal over 
    //   links and joints starting from base, using following functions: 
    //     traverseFKBase
    //     traverseFKLink
    //     traverseFKJoint
    //
    // user interface needs the heading (z-axis) and lateral (x-axis) directions
    //   of robot base in world coordinates stored as 4x1 matrices in
    //   global variables "robot_heading" and "robot_lateral"
    //
    // if geometries are imported and using ROS coordinates (e.g., fetch),
    //   coordinate conversion is needed for kineval/threejs coordinates:
    //

function traverseFKLink(link,xform){
    //buildFKTransforms(link,xform);
    var i;
    
    link.xform=xform;
    if(link.hasOwnProperty('children')){
        for(i=0;i<link.children.length;i++){
            var x = link.children[i];
            if(xform=='undefined')
                prompt();
            traverseFKJoint(robot.joints[x],xform);
        }
    }
}

function traverseFKJoint(joint,xform){
    //console.log(joint.xyz);
    //if (robot.links_geom_imported){
        //var rotMat=specialRotate(0,-Math.PI/2,-Math.PI/2);
        
    //}
    buildFKTransforms(joint,xform);
    if(xform=='undefined')
            prompt();
    if(joint.hasOwnProperty('child')){
        if(joint.xform=='undefined')
            prompt();
        traverseFKLink(robot.links[joint.child],joint.xform);
    }
}

function traverseFKBase(base,xform){
    //buildFKTransforms(base,xform);
    //console.log("Base")
    var i,heading,lateral;
    
    
    base_disp=base.xform[3];
    base.xform=xform;
    //console.log(robot_lateral);
    if(robot.links_geom_imported){
        heading = matrix_multiply(base.xform,[[1],[0],[0],[1]]);
        lateral = matrix_multiply(base.xform,[[0],[1],[0],[1]]);
    }
    else{
        heading = matrix_multiply(base.xform,[[0],[0],[1],[1]]);
        lateral = matrix_multiply(base.xform,[[1],[0],[0],[1]]);
    }
    
    robot_heading=heading;
    //console.log(robot_heading);
    robot_lateral=lateral;

    for(i=0;i<base.children.length;++i){
        x = base.children[i];
        traverseFKJoint(robot.joints[x],base.xform);
    }
}


function specialRotate(rx,ry,rz){

    Rx=generate_rotation_matrix_X(rx);
    //console.log(Rx)
    Ry=generate_rotation_matrix_Y(ry);
    //console.log(Ry)
    Rz=generate_rotation_matrix_Z(rz);
    //console.log(Rz)
    //prompt();
    
    trans=matrix_multiply(Ry,Rx);
    trans1=matrix_multiply(Rz,trans);
    //trans2=matrix_multiply(T,trans1)
    //trans3=matrix_multiply(trans2,xform)
    //RyRx=matrix_multiply(Ry,Rx);
    //R=matrix_multiply(Rz,RyRx);
    //console.log(TRz)
    
    //console.log(RyRx);
    //transform=matrix_multiply(T,R);
    //console.log(transform);
    return trans1;
}