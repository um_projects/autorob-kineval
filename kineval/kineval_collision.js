
/*-- |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/|

    KinEval | Kinematic Evaluator | collision detection

    Implementation of robot kinematics, control, decision making, and dynamics 
        in HTML5/JavaScript and threejs
     
    @author ohseejay / https://github.com/ohseejay / https://bitbucket.org/ohseejay

    Chad Jenkins
    Laboratory for Perception RObotics and Grounded REasoning Systems
    University of Michigan

    License: Creative Commons 3.0 BY-SA

|\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| --*/

// KE: merge collision test into FK
// KE: make FK for a configuration and independent of current robot state

kineval.robotIsCollision = function robot_iscollision() {
    // test whether geometry of current configuration of robot is in collision with planning world 

    // form configuration from base location and joint angles
    var q_robot_config = [
        robot.origin.xyz[0],
        robot.origin.xyz[1],
        robot.origin.xyz[2],
        robot.origin.rpy[0],
        robot.origin.rpy[1],
        robot.origin.rpy[2]
    ];

    q_names = {};  // store mapping between joint names and q DOFs

    for (x in robot.joints) {
        q_names[x] = q_robot_config.length;
        q_robot_config = q_robot_config.concat(robot.joints[x].angle);
    }

    // test for collision and change base color based on the result
    collision_result = kineval.poseIsCollision(q_robot_config);

    robot.collision = collision_result;
}

function FKTransform(element,xForm,poseElement){
    var xyz,rpy,T,Rx,Ry,Rz,trans,trans1,trans2;
    
    xyz=element.origin.xyz;
    rpy=element.origin.rpy;
    //console.log("Processing now");
    //console.log(element.name);
    T=generate_translation_matrix(xyz[0],xyz[1],xyz[2]);
    //console.log(T)
    
    Rx=generate_rotation_matrix_X(rpy[0]);
    //console.log(Rx)
    Ry=generate_rotation_matrix_Y(rpy[1]);
    //console.log(Ry)
    Rz=generate_rotation_matrix_Z(rpy[2]);
    //console.log(Rz)
    //prompt();
    
    trans=matrix_multiply(Ry,Rx);
    trans1=matrix_multiply(Rz,trans);
    if (element.hasOwnProperty('type')){
        if(element.type==='continuous'||element.type==='revolute'){
            var q=quaternion_from_axisangle(element.axis,poseElement);
            //q=quaternion_normalize(q);
            var rotation=quaternion_to_rotation_matrix(q);
            trans1=matrix_multiply(trans1,rotation);
            //element.xform=matrix_multiply(xform,rotation);
        }
        else if(element.type==='prismatic'){
            //var q=quaternion_from_axisangle(element.axis,0);
            var T1=generate_translation_matrix(0,0,poseElement);
            trans1=matrix_multiply(T1,trans1);
        }
    }
    trans2=matrix_multiply(T,trans1);
    
    
    return matrix_multiply(xForm,trans2);
    
    //robot.xform
    //console.log(RyRx);
    //transform=matrix_multiply(T,R);
    //console.log(transform);
    
    //element.xform=trans3
    //console.log(element.xform);
    
}

function baseFKTransform(element, xform, pose){
    var xyz,rpy,T,Rx,Ry,Rz,trans,trans1,trans2;
    
    xyz=element.origin.xyz;
    rpy=element.origin.rpy;
    //console.log("Processing now");
    //console.log(element.name);
    T=generate_translation_matrix(pose[0],pose[1],pose[2]);
    //console.log(T)
    
    Rx=generate_rotation_matrix_X(pose[3]);
    //console.log(Rx)
    Ry=generate_rotation_matrix_Y(pose[4]);
    //console.log(Ry)
    Rz=generate_rotation_matrix_Z(pose[5]);
    //console.log(Rz)
    //prompt();
    
    trans=matrix_multiply(Ry,Rx);
    trans1=matrix_multiply(Rz,trans);
    trans2=matrix_multiply(T,trans1);

    return matrix_multiply(xform,trans2);
    

    //console.log(RyRx);
    //transform=matrix_multiply(T,R);
    //console.log(transform);
    
    //element.xform=trans3
    //console.log(element.xform);
    
}


kineval.poseIsCollision = function robot_collision_test(q) {
    // perform collision test of robot geometry against planning world 

    // test base origin (not extents) against world boundary extents
    if ((q[0]<robot_boundary[0][0])||(q[0]>robot_boundary[1][0])||(q[2]<robot_boundary[0][2])||(q[2]>robot_boundary[1][2]))
        return robot.base;

    // traverse robot kinematics to test each body for collision
    // STENCIL: implement forward kinematics for collision detection

    var base = {origin:{xyz:[q[0],q[1],q[2]], 
                        rpy:[q[3],q[4],q[5]]}

    }

    mstack=generate_identity(4);
    
    mstack = baseFKTransform(robot,mstack,q);
    if (robot.links_geom_imported){
        var rotMat = specialRotate(0,-Math.PI/2,-Math.PI/2);
        mstack=matrix_multiply(mstack,rotMat);
    }
    //mstack = robot.xform;

    //return robot_collision_forward_kinematics(q);
    return traverse_collision_forward_kinematics_link(robot.links[robot.base],mstack,q);
}

function traverse_collision_forward_kinematics_joint(joint,mstack,q){
    
    var index = q_names[joint.name];
    qElement = q[index];

    mstack=FKTransform(joint,mstack,qElement);
    if(joint.hasOwnProperty('child')){
        var local_collision = traverse_collision_forward_kinematics_link(robot.links[joint.child],mstack,q);
        return local_collision;
    }
    return false;

}

function traverse_collision_forward_kinematics_link(link,mstack,q) {

    /* test collision FK
    console.log(link);
    */
    if (typeof link.visual !== 'undefined') {
        var local_link_xform = matrix_multiply(mstack,generate_translation_matrix(link.visual.origin.xyz[0],link.visual.origin.xyz[1],link.visual.origin.xyz[2]));
    }
    else {
        var local_link_xform = matrix_multiply(mstack,generate_identity(4));
    }

    // test collision by transforming obstacles in world to link space
/*
    mstack_inv = matrix_invert_affine(mstack);
*/
    mstack_inv = numeric.inv(mstack);

    var i;
    var j;

    // test each obstacle against link bbox geometry by transforming obstacle into link frame and testing against axis aligned bounding box
    //for (j=0;j<robot_obstacles.length;j++) { 
    for (j in robot_obstacles) { 

        var obstacle_local = matrix_multiply(mstack_inv,robot_obstacles[j].location);

        // assume link is in collision as default
        var in_collision = true; 

        // if obstacle lies outside the link extents along any dimension, no collision is detected
        if (
            (obstacle_local[0][0]<(link.bbox.min.x-robot_obstacles[j].radius))
            ||
            (obstacle_local[0][0]>(link.bbox.max.x+robot_obstacles[j].radius))
        )
                in_collision = false;
        if (
            (obstacle_local[1][0]<(link.bbox.min.y-robot_obstacles[j].radius))
            ||
            (obstacle_local[1][0]>(link.bbox.max.y+robot_obstacles[j].radius))
        )
                in_collision = false;
        if (
            (obstacle_local[2][0]<(link.bbox.min.z-robot_obstacles[j].radius)) 
            ||
            (obstacle_local[2][0]>(link.bbox.max.z+robot_obstacles[j].radius))
        )
                in_collision = false;

        // if obstacle lies within link extents along all dimensions, a collision is detected and return true
        if (in_collision)
            return link.name;
    }

    // recurse child joints for collisions, returning true if child returns collisionq
    if (typeof link.children !== 'undefined') { // return if there are no children
        var local_collision;
        //for (i=0;i<link.children.length;i++) {
        for (i in link.children) {
            local_collision = traverse_collision_forward_kinematics_joint(robot.joints[link.children[i]],mstack,q);
            if (local_collision)
                return local_collision;
        }
    }

    // return false, when no collision detected for this link and children 
    return false;
}



