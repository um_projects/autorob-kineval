//////////////////////////////////////////////////
/////     MATRIX ALGEBRA AND GEOMETRIC TRANSFORMS 
//////////////////////////////////////////////////



function matrix_copy(m1) {
    // returns 2D array that is a copy of m1

    var mat = [];
    var i,j;

    for (i=0;i<m1.length;i++) { // for each row of m1
        mat[i] = [];
        for (j=0;j<m1[0].length;j++) { // for each column of m1
            mat[i][j] = m1[i][j];
        }
    }
    return mat;
}


    // STENCIL: reference matrix code has the following functions:
    //   matrix_multiply*
    //   matrix_transpose *
    //   matrix_pseudoinverse *
    //   matrix_invert_affine 
    //   vector_normalize *
    //   vector_cross
    //   generate_identity *
    //   generate_translation_matrix*
    //   generate_rotation_matrix_X*
    //   generate_rotation_matrix_Y*
    //   generate_rotation_matrix_Z*
    

sin=Math.sin;
cos=Math.cos;

function matrix_multiply(m1,m2) {
    // returns 2D array that is a m1*m2

    var mat = [];
    var i,j,k;
    var temp=0;

    for (i=0;i<m1.length;i++) { // for each row of m1
        mat[i] = [];
        for (j=0;j<m2[0].length;j++) { // for each column of m2
            temp=0;
            for (k=0;k<m1[0].length;k++){
                temp+=m1[i][k]*m2[k][j];
            }
            mat[i][j]=temp;
        }
    }
    return mat;
}
/*
function matrix_transpose(m1) {
    // returns 2D array that is a copy of m1

    var mat = [];
    var i,j;

    for (j=0;j<m1[0].length;j++) { // for each row of m1
        mat[j] = [];
        for (i=0;i<m1.length;i++) { // for each column of m1
            mat[j][i] = m1[i][j];
        }
    }
    return mat;
}
*/
function matrix_transpose(array){
    var newArray = [];
    var arrayLength = array[0].length;
    for(var i = 0; i < arrayLength; i++){
        newArray.push([]);
    };

    for(var i = 0; i < array.length; i++){
        for(var j = 0; j < arrayLength; j++){
            newArray[j].push(array[i][j]);
        };
    };

    return newArray;
}

function matrix_pseudoinverse(m){
    var p = m.length;
    //console.log(p);
    var q = m[0].length;
   //console.log(q);
    var mT = matrix_transpose(m);//[m[0][0],m[1][0],m[2][0],m[3][0],m[4][0],m[5][0]]; //matrix_transpose(m);

    if (p>q){
        var mTm = matrix_multiply(mT,m);
        var mTmInv = numeric.inv(mTm);
        var temp= matrix_multiply(mTmInv,mT);
        //console.log(temp);
        return temp
    }
    else if (p<q){
        //var mmT=mTimesmT(m,mT);//matrix_multiply(m,mT);
        var mmT=matrix_multiply(m,mT);
        var mmTInv= numeric.inv(mmT);
        //var temp=specialMatMul(mT,mmTInv);//matrix_multiply(mT,mmTInv);
        var temp=matrix_multiply(mT,mmTInv);
        return temp
    }
    else{
        return numeric.inv(m)
    }
}

function mTimesmT(m1,m2){
    var mat=[];
    
    for( var i=0; i<m1.length; i++){
        mat[i]=[];
        for(var j=0;j<m1.length; j++){
            mat[i][j]=m1[i]*m2[j];
        }
    }
    return mat;
}

function specialMatMul(m1, m2){
    var mat=[];
    for (var i=0;i<m2[0].length;i++){
        for (var j=0; j<m2.length;j++){
            mat[i]+=m1[j]*m2[j][i];
        }
    }
    return mat;
}


function generate_rotation_matrix_X(t){

    var mat=[   [   1,     0,      0,  0],
                [   0, cos(t),-sin(t), 0],
                [   0, sin(t), cos(t), 0],
                [   0,     0,      0,  1]   ];
    return mat;
}

function generate_rotation_matrix_Y(t){
    
    var mat=[   [ cos(t),  0,  sin(t),  0],
                [      0,  1,       0,  0],
                [-sin(t),  0,  cos(t),  0],
                [      0,  0,       0,  1]  ];
    return mat;
}
function generate_rotation_matrix_Z(t){
    
    var mat=[   [ cos(t),-sin(t),   0,  0],
                [ sin(t), cos(t),   0,  0],
                [      0,      0,   1,  0],
                [      0,      0,   0,  1]  ];
    return mat;
}
function generate_translation_matrix(x,y,z){
    
    var mat=[   [   1,  0,   0,  x],
                [   0,  1,   0,  y],
                [   0,  0,   1,  z],
                [   0,  0,   0,  1] ];
    
    return mat;
}

function generate_identity(order){
    var mat = [];
    
    for (i=0;i<order;i++){
        mat[i]=[];
        for (j=0;j<order;j++){
            if(i===j){
                mat[i][j]=1;
            }
            else{
                mat[i][j]=0;
            }
        }
    }
    return mat;
}

function vector_cross(m1,m2){
    var mat;
    var a1=m1[0],a2=m1[1],a3=m1[2];
    var b1=m2[0],b2=m2[1],b3=m2[2];
    mat=[(a2*b3-a3*b2),(a3*b1-a1*b3),(a1*b2-a2*b1)];
    return mat;
}

function vector_normalize(m1){
    var mat=[];
    var abs=0;
    //var pow=Math.pow
    for(var i =0; i<m1.length; i++){
        abs+=Math.pow(m1[i],2);
    }
    abs=Math.sqrt(abs)
    for(var i =0; i<m1.length; i++){
        mat.push(m1[i]/abs);
    }
    return mat;
}

function vector_dot(m1,m2){
    var len= m1.length;
    var i;
    var prod=0;
    
    for(i=0;i<len;i++){
        prod+=m1[i]*m2[i];
    }

    return prod;
}

function vector_addsub(m1,m2,add){
    var m=[];

    if (m1.length!=m2.length){
        console.log("matrix rows inconsistent");
    }

    for (var i=0;i<m1.length;i++){

        if(add){
            m[i]=m1[i]+m2[i];
        }
        else{
            m[i]=m1[i]-m2[i];
        }
    }
    
    //console.log(m);
    return m;
}

function distance(p1,p2){
    var p=0;
    
    for (var i=0; i<p1.length-1; i++){
        p+=Math.pow(p1[i]-p2[i],2);
    }

    return Math.sqrt(p);
}

function scalarMultiply(m1,s){
    var m=[];
    for (var i=0;i<m1.length;i++){
        m[i]=m1[i]*s;
    }
    return m;
}