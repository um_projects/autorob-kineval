//////////////////////////////////////////////////
/////     QUATERNION TRANSFORM ROUTINES 
//////////////////////////////////////////////////

    // STENCIL: reference quaternion code has the following functions:
    //   quaternion_from_axisangle
    //   quaternion_normalize
    //   quaternion_to_rotation_matrix
    //   quaternion_multiply
var sin=Math.sin;
var cos=Math.cos;
var pow=Math.pow;

function quaternion_from_axisangle(axis,t){
    //console.log("test")
    var b=axis[0];
    var c=axis[1];
    var d=axis[2];

    q=[cos(t/2),b*sin(t/2),c*sin(t/2),d*sin(t/2)];
    
    return q;
}

function quaternion_normalize(q){
    var normsq=pow(q[0],2)+pow(q[1],2)+pow(q[2],2)+pow(q[3],2);
    var normalized=q/Math.sqrt(normsq);
    return normalized;
}

function quaternion_to_rotation_matrix(q){

    var qw=q[0];
    var qx=q[1];
    var qy=q[2];
    var qz=q[3];


    var r11=1-2*pow(qy,2)-2*pow(qz,2);
    var r12=(2*qx*qy)-(2*qz*qw);
    var r13=(2*qx*qz)+(2*qy*qw);
    var r21=(2*qx*qy)+(2*qz*qw);
    var r22=(1-2*pow(qx,2)-2*pow(qz,2));
    var r23=(2*qy*qz)-(2*qx*qw);
    var r31=(2*qx*qz)-(2*qy*qw);
    var r32=(2*qy*qz)+(2*qx*qw);
    var r33=1-2*pow(qx,2)-2*pow(qy,2);
    var r=[ [r11,r12,r13,0],
            [r21,r22,r23,0],
            [r31,r32,r33,0],
            [  0,  0,  0,1]   ];
    //console.log(r11,r12,r13,r21,r22,r23,r31,r32,r33);
    
    return r;
}

function quaternion_multiply(q1,q2){
    
    var scalar=q1[0]*q2[0]-vector_dot(q1,q2);
    var vector=q1[0]*q2+q2[0]*q1+vector_cross(q1,q2);
    var q=[scalar,vector[0],vector[1],vector[2]];
    
    return q;
}

