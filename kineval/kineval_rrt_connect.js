
/*-- |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/|

    KinEval | Kinematic Evaluator | RRT motion planning

    Implementation of robot kinematics, control, decision making, and dynamics 
        in HTML5/JavaScript and threejs
     
    @author ohseejay / https://github.com/ohseejay / https://bitbucket.org/ohseejay

    Chad Jenkins
    Laboratory for Perception RObotics and Grounded REasoning Systems
    University of Michigan

    License: Creative Commons 3.0 BY-SA

|\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| --*/

//////////////////////////////////////////////////
/////     RRT MOTION PLANNER
//////////////////////////////////////////////////

// STUDENT: 
// compute motion plan and output into robot_path array 
// elements of robot_path are vertices based on tree structure in tree_init() 
// motion planner assumes collision checking by kineval.poseIsCollision()

/* KE 2 : Notes:
   - Distance computation needs to consider modulo for joint angles
   - robot_path[] should be used as desireds for controls
   - Add visualization of configuration for current sample
   - Add cubic spline interpolation
   - Add hook for random configuration
   - Display planning iteration number in UI
*/

/*
STUDENT: reference code has functions for:

*/

kineval.planMotionRRTConnect = function motionPlanningRRTConnect() {
    eps=1
    // exit function if RRT is not implemented
    //   start by uncommenting kineval.robotRRTPlannerInit 
    if (typeof kineval.robotRRTPlannerInit === 'undefined') return;

    if ((kineval.params.update_motion_plan) && (!kineval.params.generating_motion_plan)) {
        kineval.robotRRTPlannerInit();
        kineval.params.generating_motion_plan = true;
        kineval.params.update_motion_plan = false;
        kineval.params.planner_state = "initializing";
    }
    if (kineval.params.generating_motion_plan) {
        rrt_result = robot_rrt_planner_iterate();
        if (rrt_result === "reached") {
            kineval.params.update_motion_plan = false; // KE T needed due to slight timing issue
            kineval.params.generating_motion_plan = false;
            custom_goal=false;
            textbar.innerHTML = "planner execution complete";
            kineval.params.planner_state = "complete";
        }
        else kineval.params.planner_state = "searching";
    }
    else if (kineval.params.update_motion_plan_traversal||kineval.params.persist_motion_plan_traversal) {

        if (kineval.params.persist_motion_plan_traversal) {
            kineval.motion_plan_traversal_index = (kineval.motion_plan_traversal_index+1)%kineval.motion_plan.length;
            textbar.innerHTML = "traversing planned motion trajectory";
        }
        else
            kineval.params.update_motion_plan_traversal = false;

        // set robot pose from entry in planned robot path
        robot.origin.xyz = [
            kineval.motion_plan[kineval.motion_plan_traversal_index].vertex[0],
            kineval.motion_plan[kineval.motion_plan_traversal_index].vertex[1],
            kineval.motion_plan[kineval.motion_plan_traversal_index].vertex[2]
        ];

        robot.origin.rpy = [
            kineval.motion_plan[kineval.motion_plan_traversal_index].vertex[3],
            kineval.motion_plan[kineval.motion_plan_traversal_index].vertex[4],
            kineval.motion_plan[kineval.motion_plan_traversal_index].vertex[5]
        ];

        // KE 2 : need to move q_names into a global parameter
        for (x in robot.joints) {
            robot.joints[x].angle = kineval.motion_plan[kineval.motion_plan_traversal_index].vertex[q_names[x]];
        }

    }
}


    // STENCIL: uncomment and complete initialization function
kineval.robotRRTPlannerInit = function robot_rrt_planner_init() {
    rrt_alg = 1;  // 0: basic rrt (OPTIONAL), 1: rrt_connect (REQUIRED), 2: RRT-Star
    // form configuration from base location and joint angles
    q_start_config = [
        robot.origin.xyz[0],
        robot.origin.xyz[1],
        robot.origin.xyz[2],
        robot.origin.rpy[0],
        robot.origin.rpy[1],
        robot.origin.rpy[2]
    ];

    q_names = {};  // store mapping between joint names and q DOFs
    q_index = [];  // store mapping between joint names and q DOFs

    
    for (x in robot.joints) {
        q_names[x] = q_start_config.length;
        q_index[q_start_config.length] = x;
        q_start_config = q_start_config.concat(robot.joints[x].angle);
    }

    // set goal configuration as the zero configuration
    var i; 
    if(typeof custom_goal == 'undefined'){
        q_goal_config = new Array(q_start_config.length);
        for (i=0;i<q_goal_config.length;i++) q_goal_config[i] = 0;
    }

    T_a=tree_init(q_start_config);
    T_b=tree_init(q_goal_config);

    // flag to continue rrt iterations
    rrt_iterate = true;
    rrt_iter_count = 0;

    // make sure the rrt iterations are not running faster than animation update
    cur_time = Date.now();
}

/////////////////////////////////
// Custom RRT Support functions//
/////////////////////////////////

function random_config(size){
    //var excluded = [1,3,5]; //Y, Roll, Yaw
    
    var rTemp=[];
    var scaleX = robot_boundary[1][0]-robot_boundary[0][0];
    var scaleZ = robot_boundary[1][2]-robot_boundary[0][2];
    var offsetX = (robot_boundary[1][0]+robot_boundary[0][0])/2;
    var offsetZ = (robot_boundary[1][2]+robot_boundary[0][2])/2;

    for(var i=0; i<size; i++){
        rTemp.push(0);
    }

    var randT=(Math.random()-0.5)*scaleX;
    rTemp[0]=randT+offsetX;
    rTemp[1]=0;
    var randT=(Math.random()-0.5)*scaleZ;
    rTemp[2]=randT+offsetZ;
    rTemp[3]=0;
    var randT=(Math.random()-0.5)*3.14;
    rTemp[4]=randT;
    rTemp[5]=0;
    
    for (var joint in q_names){
        var index = q_names[joint];
    
        if (robot.joints[joint].type!="fixed"){
            var randT=(Math.random()-0.5)*3.14;
            if(robot.joints[joint].hasOwnProperty("limit")){
                if(randT<robot.joints[joint].limit.lower){
                    randT=robot.joints[joint].limit.lower;
                }
                else if(randT>robot.joints[joint].limit.upper){
                    randT=robot.joints[joint].limit.upper;
                }
                rTemp[index]=randT;
            }
        }
    }
    
/*
    for(var j=0; j<excluded.length; j++){
        rand[excluded[j]]=0;
    }
  */      
    return rTemp;
}

function pointAlongVector(rand,origin){
    //input world coordinates
    //console.log(origin);
    var diff = vector_addsub(rand,origin,false);
    diff = vector_normalize(diff);
    diff = scalarMultiply(diff,eps);
    //var dist=eps;
    //console.log(dist);
    //console.log(i);
    var coord = vector_addsub(diff,origin,true);
    return coord;
}

function nearestNeighbour(tree,qCoord){
    var minDist = 10000;
    //var xCoord = tree.vertices[0].vertex;
    var temp = tree.vertices[0].vertex;
    //var world_origin=temp;
    var nearest = temp; //initialise
    //console.log(tree);
    for(var i=tree.vertices.length-1; i>=0; i--){

        var xCoord=tree.vertices[i].vertex;
        var dist = distance(xCoord,qCoord);
        if (minDist>dist){
            minDist=dist;
            nearest = xCoord;
            //w=G[Math.round(qCoord[0]/eps)+world_origin_grid[0]][Math.round(qCoord[1]/eps)+world_origin_grid[1]];
            //console.log(originGrid);
            //originCoord=[originGrid.i,originGrid.j];
            parent=i;
        }
    }
    //print(world_origin)
    //console.log("Nearest Point");
    //console.log(w);
    return nearest;//G[nearest[0]][nearest[1]];
}

function generatePath(tree){
    
    //var index=tree.newest;
    var v = tree.vertices[tree.newest].edges[0];
    //var path=initRRT(v.vertex);
    var path = [];
    path.push(v);
    while (true){
        var v = v.edges[0];
        var vCoord = v.vertex;
        //insertTreeVertex(path,vCoord);
        path.push(v);
        //var v=G[Math.round(vCoord[0]/eps)+world_origin_grid[0]][Math.round(vCoord[1]/eps)+world_origin_grid[1]];
        //index=v.parent;
        if (v.vertex==q_init){
            break;
        }
    }
    return path;
}

function generateConnectPath(tree1,tree2){
    // prompt();
    //var index=tree.newest;
    var v = tree1.vertices[tree1.newest];
    //var path=initRRT(v.vertex);
    var path = [];
    path.push(v);
    while (true){
        var v= v.edges[0];
        var vCoord = v.vertex;
        //insertTreeVertex(path,vCoord);
        path.push(v);
        //var v=G[Math.round(vCoord[0]/eps)+world_origin_grid[0]][Math.round(vCoord[1]/eps)+world_origin_grid[1]];
        //index=v.parent;
        if (v.vertex==q_start_config || v.vertex == q_goal_config){
            break;
        }
    }

    var v = tree2.vertices[tree2.newest];
    //var path=initRRT(v.vertex);
    // var path2 = [];
    path.unshift(v);
    while (true){
        var v = v.edges[0];
        var vCoord = v.vertex;
        //insertTreeVertex(path,vCoord);
        path.unshift(v);
        //var v=G[Math.round(vCoord[0]/eps)+world_origin_grid[0]][Math.round(vCoord[1]/eps)+world_origin_grid[1]];
        //index=v.parent;
        if (v.vertex==q_start_config || v.vertex == q_goal_config){
            break;
        }
    }
    // var path = path1.concat(path2);
    return path;
}

function extendTowards(tree, q_target){

    var nearest = nearestNeighbour(tree, q_target)
    //var nearest = tree.vertices[tree.newest].vertex;
    var qWorld = pointAlongVector(q_target,nearest);
    // var q=G[qCoord[0]][qCoord[1]];
    // var qWorld=[q.x,q.y];

    if (kineval.poseIsCollision(qWorld)){
        return "Collision";
    }

    tree_add_vertex(tree, qWorld, parent);
    // insertTreeEdge(tree,tree.newest-1,tree.newest);
    tree_add_edge(tree,parent,tree.newest);
    
    if(distance(qWorld,q_target)<eps/2){
        //prompt();
        return "Reached";
    }
    return "Advance";
}

function treeSwap(){
    var treeSet = [T_a,T_b];
    return treeSet;
}

function findNeighbours(tree, point){
    var neighbours=[];
    for (var i=tree.newest;i>=0;i--){
        var v=tree.vertices[i];
        var dist = distance(point,v.vertex);
        if(dist<eps*1.5){
            neighbours.push(i);
        }
    }
    return neighbours;
}

function checkNeighbours(tree, qCoord, neighbours, parIndex){
    var nearest = tree.vertices[parIndex];
    var vCoord = nearest.vertex;
    var minCost = nearest.cost + distance(qCoord,vCoord);
    var new_parent = parIndex;
    for (var i=0; i<neighbours.length;i++){
        var n=neighbours[i];
        var v=tree.vertices[n];
        var vCoord=v.vertex;
        var cost = v.cost + distance(qCoord,vCoord);
        if(cost<minCost){
            minCost = cost;
            new_parent = n;
        }
    }
    return new_parent;    
}

function reWire(tree, neighbours){
    var new_node = tree.vertices[tree.newest];
    for(var i=0; i<neighbours.length; i++){
        n=neighbours[i];
        var v=tree.vertices[n];
        var new_cost = new_node.cost + distance(new_node.vertex,v.vertex);
        if(v.cost>new_cost){
            v.parent=tree.newest;
        }
    }
}

function generateStarPath(tree){
    var path=[];
    var index = tree.newest;
    var v = tree.vertices[index]
    while(true){
        path.unshift(v);
        index=v.parent;
        var v = tree.vertices[index]
        if(!v.hasOwnProperty("parent")){ //start node has no parent
            path.unshift(v);
            break;
        }
    }
    return path;
}

function drawHighlightedPath(path){

    for (i=1;i<path.length;i++) {
        path[i].geom.material.color = {r:1,g:0,b:0};
    }
}

function createFormattedPath(rawpath){
    var tree = tree_init(q_goal_config);
    //var q;
    for (var i=0;i<rawpath.length;i++){
        tree_add_vertex(tree, rawpath[i], tree.newest);
    }
    return tree.vertices;
}

function generate_cubic_spline(initial_wp, final_wp, T){
    //   Description of spline and coeffs
    //   q = a0 + a1*(t) + a2*(t^2) + a3*(t^3)
    //   a = [a0  a1  a2  a3] (column vector)
    
    var t0 = 0;
    var tf = T;
    var q0 = initial_wp;
    var qf = final_wp;
    var v0 = 0;
    var vf = 0;
    var p = Math.pow;
    var b = [[q0,v0,qf,vf]];
    var M = [   [1,t0,  p(t0,2),   p(t0,3)],
                [0, 1,     2*t0, 3*p(t0,2)],
                [1,tf,  p(tf,2),   p(tf,3)],
                [0, 1,     2*tf, 3*p(tf,2)] ];
    var Minv = numeric.inv(M);
    var a = matrix_multiply(Minv,matrix_transpose(b));

    return a
}

function generateCubicPlan(path){
    var newPath = [];
    var pos_plan = [];
    var dt=1;
    var T=5;
    var iterations = Math.floor(T/dt);
    var p=Math.pow;


    for(var i=0;i<path.length-1;i++){   
        var initial_wp=path[i];
        var final_wp=path[i+1];
        pos_plan=[];
        
        for (var index = 0;index<6;index++){
            if(index%2==0){
                var a = generate_cubic_spline(initial_wp.vertex[index],final_wp.vertex[index],T)
                var pos = [];
                //vel = [];
                //calculate position and velocity commands for every dt
                for(var j=0; j< iterations; j++){
                    t = j*dt;
                    //these store each position or velocity calculation in array pos and vel respectively, for given time and joint
                    pos.push(a[0][0]    +   a[1][0]*t  +     (a[2][0]*p(t,2)) +       (a[3][0]*p(t,3)));
                    //vel.push(            a[1]    +       (2*a[2]*t)  +     (3*a[3]*(t**2)))    
                }
            }
            else{
                var pos=[];
                for(var j=0; j< Math.floor(T/dt); j++){
                    pos.push(0);
                }
            }
            //Adds this joint's plan to the overall 2D array of joints
            //vel_plan.push(vel);
            pos_plan.push(pos);
        }

        for (var joint in q_names){
            var index = q_names[joint];
            if (robot.joints[joint].type!="fixed"){
                //set of coefficients for each joint's cubic spline
                var a = generate_cubic_spline(initial_wp.vertex[index],final_wp.vertex[index],T);
                var pos = [];
                //vel = [];
                //calculate position and velocity commands for every dt
                for(var j=0; j< iterations; j++){
                    t = j*dt;
                    //these store each position or velocity calculation in array pos and vel respectively, for given time and joint
                    pos.push(a[0][0]    +   a[1][0]*t  +     (a[2][0]*p(t,2)) +       (a[3][0]*p(t,3)));
                    //vel.push(            a[1]    +       (2*a[2]*t)  +     (3*a[3]*(t**2)))    
                }
                //Adds this joint's plan to the overall 2D array of joints
                //vel_plan.push(vel);
                pos_plan.push(pos);
            }
            else{
                var pos = [];
                for(var j=0; j< Math.floor(T/dt); j++){
                    pos.push(0);
                }
                pos_plan.push(pos);
            }
        }
        
        var newSubPath=matrix_transpose(pos_plan);
        for (var k=0; k<newSubPath.length; k++){
            newPath.push(newSubPath[k]);
        }
    }
    newPath=createFormattedPath(newPath);

    return newPath;
}




function robot_rrt_planner_iterate() {

    //var i;
    

    if (rrt_iterate && (Date.now()-cur_time > 10)) {
        cur_time = Date.now();

    // STENCIL: implement single rrt iteration here. an asynch timing mechanism 
    //   is used instead of a for loop to avoid blocking and non-responsiveness 
    //   in the browser.
    //
    //   once plan is found, highlight vertices of found path by:
    //     tree.vertices[i].vertex[j].geom.material.color = {r:1,g:0,b:0};
    //
    //   provided support functions:
    //
    //   kineval.poseIsCollision - returns if a configuration is in collision
    //   tree_init - creates a tree of configurations
    //   tree_add_vertex - adds and displays new configuration vertex for a tree
    //   tree_add_edge - adds and displays new tree edge between configurations
    if(rrt_alg==1){ 

        //RRT CONNECT
        
            qRand=random_config(q_goal_config.length); 
            //var q=G[qCoord[0]][qCoord[1]];
            //var qWorldCoord = [q.x, q.y];
            //console.log(qWorldCoord);
            
            //nearest = nearestNeighbour(T_a,qCoord);

            //here nearest is just the last node
            //var nearest = T_a.vertices[T_a.vertices.length - 1].vertex;
            
            //console.log(nearestCoord);
            var nearest = nearestNeighbour(T_a,qRand);
            //console.log(nearest);
            //q = G[qCoord[0]][qCoord[1]];
            //console.log(q);
            var qCoord = pointAlongVector(qRand,nearest);

            if (kineval.poseIsCollision(qCoord)){
                var treeSet = treeSwap();
                T_a=treeSet[1];
                T_b=treeSet[0];
                return "iterating";
            }
        

        //testCollision()
        console.log("adding");
        //qWorldCoord = [q.x,q.y];
        tree_add_vertex(T_a, qCoord, parent);
        tree_add_edge(T_a,parent,T_a.newest);

        var status = extendTowards(T_b,qCoord);
        while (status==="Advance"){
            status = extendTowards(T_b,qCoord);
        }

        if (status === "Reached"){
            // prompt();
            console.log("reached");
            //TODO
            var path = generateConnectPath(T_a,T_b);
            if(kineval.params.cubic_spline)
                path = generateCubicPlan(path);
            drawHighlightedPath(path);
            kineval.motion_plan=path;
            //found=true;
            rrt_iterate=false;
            return "reached";
        }
        else{
            var treeSet = treeSwap();
            T_a=treeSet[1];
            T_b=treeSet[0];
        }

        //prompt();

    /*
        if(Math.abs(q.x-q_goal[0])<(eps/2) && Math.abs(q.y-q_goal[1])<(eps/2)){
            console.log("reached");

            var path = generatePath(T_a);
            drawHighlightedPath(path);
            //found=true;
            search_iterate=false;
            return "succeeded";
        }
    */
    // prompt();
        return "iterating";
    }
    else if(rrt_alg=2){
       
            qRand=random_config(q_goal_config.length);  
            //console.log(qRand);
            //var q=G[qCoord[0]][qCoord[1]];
            //var qWorldCoord = [q.x, q.y];
            //console.log(qWorldCoord);
            
            //nearest = nearestNeighbour(T_a,qCoord);
    
            //here nearest is just the last node
            //var nearest = T_a.vertices[T_a.vertices.length - 1].vertex;
            
            //console.log(nearestCoord);
            var nearest = nearestNeighbour(T_a,qRand);
            //console.log(nearest);
            //q = G[qCoord[0]][qCoord[1]];
            //console.log(q);
            var qCoord = pointAlongVector(qRand,nearest);
            //q=G[qCoord[0]][qCoord[1]];
            //console.log(qCoord);
        
        if(kineval.poseIsCollision(qCoord)){
            return "iterating";
        }
    
        //testCollision()
        console.log("adding");
        //qWorldCoord = [q.x,q.y];
        var neighbours=[];
        if(T_a.vertices.length>1){
            neighbours = findNeighbours(T_a, qCoord);
            parent = checkNeighbours(T_a, qCoord, neighbours, parent); //parent is the index of the nearest node.
            //reWire(T_a);
        }

        tree_add_vertex(T_a, qCoord, parent);
        tree_add_edge(T_a,parent,T_a.newest);
    
        if(T_a.vertices.length>2){
            reWire(T_a, neighbours);
        }

        //Math.abs(qCoord[0]-q_goal[0])<(eps/2) && Math.abs(qCoord[2]-q_goal[1])<(eps/2)
        var dist = distance(qCoord,q_goal_config);
        if(dist<eps){
            console.log("reached");
    
            var path = generateStarPath(T_a);
            if(kineval.params.cubic_spline)
                path = generateCubicPlan(path);
            drawHighlightedPath(path);
            kineval.motion_plan=path;
            //found=true;
            rrt_iterate=false;
            return "reached";
        }
        
       // prompt();
        return "iterating";
    
    }


    }
}
//////////////////////////////////////////////////
/////     STENCIL SUPPORT FUNCTIONS
//////////////////////////////////////////////////

function tree_init(q) {

    // create tree object
    var tree = {};

    // initialize with vertex for given configuration
    tree.vertices = [];
    tree.vertices[0] = {};
    tree.vertices[0].vertex = q;
    tree.vertices[0].edges = [];
    tree.vertices[0].cost=0;

    // create rendering geometry for base location of vertex configuration
    add_config_origin_indicator_geom(tree.vertices[0]);

    // maintain index of newest vertex added to tree
    tree.newest = 0;

    return tree;
}

function tree_add_vertex(tree,q, parIndex) {


    // create new vertex object for tree with given configuration and no edges
    var new_vertex = {};
    new_vertex.edges = [];
    new_vertex.vertex = q;
    new_vertex.parent = parIndex
    var par = tree.vertices[parIndex];
    new_vertex.cost = par.cost+distance(q,par.vertex);

    // create rendering geometry for base location of vertex configuration
    add_config_origin_indicator_geom(new_vertex);

    // maintain index of newest vertex added to tree
    tree.vertices.push(new_vertex);
    tree.newest = tree.vertices.length - 1;
}

function add_config_origin_indicator_geom(vertex) {

    // create a threejs rendering geometry for the base location of a configuration
    // assumes base origin location for configuration is first 3 elements 
    // assumes vertex is from tree and includes vertex field with configuration

    temp_geom = new THREE.CubeGeometry(0.1,0.1,0.1);
    temp_material = new THREE.MeshLambertMaterial( { color: 0xffff00, transparent: true, opacity: 0.7 } );
    temp_mesh = new THREE.Mesh(temp_geom, temp_material);
    temp_mesh.position.x = vertex.vertex[0];
    temp_mesh.position.y = vertex.vertex[1];
    temp_mesh.position.z = vertex.vertex[2];
    scene.add(temp_mesh);
    vertex.geom = temp_mesh;
}


function tree_add_edge(tree,q1_idx,q2_idx) {

    // add edge to first vertex as pointer to second vertex
    tree.vertices[q1_idx].edges.push(tree.vertices[q2_idx]);

    // add edge to second vertex as pointer to first vertex
    tree.vertices[q2_idx].edges.push(tree.vertices[q1_idx]);

    // can draw edge here, but not doing so to save rendering computation
}

//////////////////////////////////////////////////
/////     RRT IMPLEMENTATION FUNCTIONS
//////////////////////////////////////////////////


    // STENCIL: implement RRT-Connect functions here, such as:
    //   rrt_extend
    //   rrt_connect
    //   random_config
    //   new_config
    //   nearest_neighbor
    //   normalize_joint_state
    //   find_path
    //   path_dfs










