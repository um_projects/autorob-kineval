
/*-- |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/|

    KinEval | Kinematic Evaluator | arm servo control

    Implementation of robot kinematics, control, decision making, and dynamics 
        in HTML5/JavaScript and threejs
     
    @author ohseejay / https://github.com/ohseejay / https://bitbucket.org/ohseejay

    Chad Jenkins
    Laboratory for Perception RObotics and Grounded REasoning Systems
    University of Michigan

    License: Creative Commons 3.0 BY-SA

|\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| |\/| --*/

kineval.setpointDanceSequence = function execute_setpoints() {

    // if update not requested, exit routine
    if (!kineval.params.update_pd_dance) return; 

    // STENCIL: implement FSM to cycle through dance pose setpoints
    var not_complete=0;
        for (var x in robot.joints) {
            if (Math.abs(kineval.params.setpoint_target[x]-robot.joints[x].angle)>0.05){
                not_complete=1;
            }
        }

        if (!not_complete){
            var i=kineval.params.dance_sequence_index[kineval.params.dance_pose_index];
            var pose=kineval.setpoints[i];
            //console.log(i)

            for (var x in robot.joints) {
                kineval.params.setpoint_target[x] = pose[x];
                //console.log(pose[x])
            }

            if(kineval.params.dance_sequence_index.length<kineval.params.dance_pose_index+2){
                kineval.params.dance_pose_index=0;
            }
            else{
                kineval.params.dance_pose_index+=1;
            }
        }

}

kineval.setpointClockMovement = function execute_clock() {

    // if update not requested, exit routine
    if (!kineval.params.update_pd_clock) return; 

    var curdate = new Date();
    for (var x in robot.joints) {
        kineval.params.setpoint_target[x] = curdate.getSeconds()/60*2*Math.PI;
    }
}


kineval.robotArmControllerSetpoint = function robot_pd_control () {

    // if update not requested, exit routine
    if ((!kineval.params.update_pd)&&(!kineval.params.persist_pd)) return; 

    kineval.params.update_pd = false; // if update requested, clear request and process setpoint control

    // STENCIL: implement P servo controller over joints
    for (x in robot.joints){
        var joint=robot.joints[x];
        joint.control=joint.servo.p_gain*(kineval.params.setpoint_target[x]-joint.angle);
    }
}


