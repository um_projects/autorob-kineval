/*|\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\
|\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/|
||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/
/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\

    Heap Sort Stencil | JavaScript support functions

    Quick JavaScript Code-by-Example Tutorial 
     
    @author ohseejay / https://github.com/ohseejay
                     / https://bitbucket.org/ohseejay

    Chad Jenkins
    Laboratory for Perception RObotics and Grounded REasoning Systems
    University of Michigan

    License: Michigan Honor License 

|\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/|
||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/
/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\
\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/*/

//Modified by Anand Asokan to implement MinHeapSort.

// create empty object 
minheaper = {}; 

// define insert function for min binary heap
function minheap_insert(heap, new_element) {
    
    heap.push(new_element);
    n=heap.length-1;
    if(n>0){
        i=Math.floor((n-1)/2)
        heapInsert(heap,n)
    }
}

// assign insert function within minheaper object
minheaper.insert = minheap_insert;

//Compare child with parent. Move upward if child is smaller than parent
function heapInsert(heap,n){
    i=Math.floor((n-1)/2);
    //console.log(i);
    
    if(heap[n].priority<=heap[i].priority){
        [heap[i],heap[n]] = [heap[n],heap[i]];
        //console.log("Swapped "+heap[i]+','+heap[n]);
        //console.log("Index: "+ i+','+n)
        if(i>=1){
        heapInsert(heap,i);
        }   
    }
}

//Compare parent with children. If Parent is larger, move downward.
function heapRemove(heap,n){
    i1=2*n+1;
    i2=2*n+2;
    i=n;
    if (i1<heap.length && heap[i].priority>heap[i1].priority)
        i=i1;
    if(i2<heap.length && heap[i].priority>heap[i2].priority)
        i=i2;
    if(i!=n){
        [heap[i],heap[n]] = [heap[n],heap[i]];
        heapRemove(heap,i);
    }
}

// define extract function for min binary heap
function minheap_extract(heap) {
    
    n=heap.length-1;
    i=Math.floor((n-1)/2);
    [heap[0],heap[n]] = [heap[n],heap[0]];
    min=heap.pop();
    if(heap.length>0){
        heapRemove(heap,0);
    }
    return min;
}

// assign extract function within minheaper object
minheaper.extract = minheap_extract;


/////// DEPTH FIRST SEARCH
DFS=[];
DFS.insert=DFS_insert
function DFS_insert(visit_queue, new_element){
    visit_queue.push(new_element);
}
DFS.extract=DFS_extract
function DFS_extract(visit_queue){
    element=visit_queue.pop();
    return element;
}

/////// BREADTH FIRST SEARCH
BFS=[];
BFS.insert=BFS_insert
function BFS_insert(visit_queue, new_element){
    visit_queue.push(new_element);
}
BFS.extract=BFS_extract
function BFS_extract(visit_queue){
    element=visit_queue.shift();
    return element;
}