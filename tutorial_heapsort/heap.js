/*|\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\
|\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/|
||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/
/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\

    Heap Sort Stencil | JavaScript support functions

    Quick JavaScript Code-by-Example Tutorial 
     
    @author ohseejay / https://github.com/ohseejay
                     / https://bitbucket.org/ohseejay

    Chad Jenkins
    Laboratory for Perception RObotics and Grounded REasoning Systems
    University of Michigan

    License: Michigan Honor License 

|\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/|
||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/
/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\
\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/||\/*/


// create empty object 
minheaper = {}; 

// define insert function for min binary heap
function minheap_insert(heap, new_element) {
    heap.push(new_element);
    n=heap.length-1;
    if(n>0){
        i=Math.floor((n-1)/2)
        heapInsert(heap,n)
    }
}

// assign insert function within minheaper object
minheaper.insert = minheap_insert;
/* Note: because the minheap_insert function is an object, we can assign 
      a reference to the function within the minheap object, which can be called
      as minheap.insert
*/

//Compare child with parent. Move upward if child is smaller than parent
function heapInsert(heap,n){
    i=Math.floor((n-1)/2);
    console.log(heap[i]+','+heap[n]);
    if(heap[n]<=heap[i]){
        [heap[i],heap[n]] = [heap[n],heap[i]];
        console.log("Swapped "+heap[i]+','+heap[n]);
        //console.log("Index: "+ i+','+n);
        //console.log(string);
        if(i>=1)
            heapInsert(heap,i);
    }    
}

//Compare parent with children. If Parent is larger, move downward.
function heapRemove(heap,n){
    i1=2*n+1;
    i2=2*n+2;
    i=n;
    if (i1<heap.length && heap[i]>heap[i1])
        i=i1;
    if(i2<heap.length && heap[i]>heap[i2])
        i=i2;
    if(i!=n){
        [heap[i],heap[n]] = [heap[n],heap[i]];
        heapRemove(heap,i);
    }
}

// define extract function for min binary heap
function minheap_extract(heap) {
    n=heap.length-1;
    i=Math.floor((n-1)/2);
    [heap[0],heap[n]] = [heap[n],heap[0]];
    smallest=heap.pop();
    //heapify(heap,n,i)
    if(heap.length>0)
        heapRemove(heap,0);
    return smallest;
    // STENCIL: implement your min binary heap extract operation
}

// assign extract function within minheaper object
minheaper.extract = minheap_extract;
    // STENCIL: ensure extract method is within minheaper object






