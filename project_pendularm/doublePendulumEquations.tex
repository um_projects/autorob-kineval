\documentclass[12pt, a4paper]{article}
\usepackage[margin=3cm]{geometry}
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{setspace}
\usepackage{verbatim}
\usepackage{subcaption}
\linespread{1.4}
\pagestyle{headings}
%\linespread{1}
\begin{document}

\pagenumbering{arabic}

\section*{\centering \bf Double Pendulum}
\section*{\centering Equations of Motion}

\section{Problem statement and parameters}

A pendulum is essentially a non-zero mass suspended through a massless rigid rod from a fixed/moving surface. This concept is extended in the case of a double pendulum, where another non-zero mass is suspended from a rigid rod from the first pendulum's mass. In an ideal scenario, the mass of the pendulum is considered to be concentrated at a point at the end of the rigid support, i.e. it behaves as a point mass. A single pendulum and double pendulum are shown in Fig. \ref{fig:single} and in Fig. \ref{fig:double} respectively. Pendula are allowed to rotate freely about the anchor point axis. A single pendulum has a predictable motion for a given set of initial conditions, but a double pendulum exhibits an unpredictable (chaotic) behaviour, $\mu$ which highly depends on the initial conditions. 

\begin{figure}[h]
	\centering
  	\includegraphics[width=0.5\linewidth]{images/single.png}
  	\caption{Simple Pendulum \cite{c1}}
  	\label{fig:single}
\end{figure}

\begin{figure}[h]
	\centering
  	\includegraphics[width=0.4\linewidth]{images/double.png}
  	\caption{Double Pendulum \cite{c2}}
  	\label{fig:double}
\end{figure}

The specifications of the double pendulum are mentioned in the figures above. $\theta_{1}$ and $\theta_{2}$ are the joint angles of each pendulum with respect to the vertical.

\section{Derivation}

The positions of the pendulum masses can be described by the following equations, by resolving along the x and y axis.

\begin{equation}
\begin{aligned} x_{1} &=l_{1} \sin \theta_{1} \\ y_{1} &=-l_{1} \cos \theta_{1} \\ x_{2} &=l_{1} \sin \theta_{1}+l_{2} \sin \theta_{2} \\ y_{2} &=-l_{1} \cos \theta_{1}-l_{2} \cos \theta_{2} \end{aligned}
\end{equation}

We can write the Potential (P) and Kinetic Energy (K) of the system as follows.

\begin{equation}
\begin{aligned} P &=m_{1} g y_{1}+m_{2} g y_{2} \\ &=-\left(m_{1}+m_{2}\right) g l_{1} \cos \theta_{1}-m_{2} g l_{2} \cos \theta_{2}\\ \\ K &=\frac{1}{2} m_{1} v_{1}^{2}+\frac{1}{2} m_{2} v_{2}^{2} \\
				  &=\frac{1}{2} m_{1} l_{1}^{2} \dot{\theta}_{1}^{2}+\frac{1}{2} m_{2}\left[l_{1}^{2} \dot{\theta}_{1}^{2}+l_{2}^{2} \dot{\theta}_{2}^{2}+2 l_{1} l_{2} \dot{\theta}_{1} \dot{\theta}_{2} \cos \left(\theta_{1}-\theta_{2}\right)\right] \end{aligned}
\end{equation}

Now, we know that the Lagrangian is
\begin{equation}
L \equiv K - P 
\end{equation}

And hence,

\begin{equation}
\begin{aligned}
L = &\frac{1}{2}\left(m_{1}+m_{2}\right) l_{1}^{2} \dot{\theta}_{1}^{2}+\frac{1}{2} m_{2} l_{2}^{2} \dot{\theta}_{2}^{2}+m_{2} l_{1} l_{2} \dot{\theta}_{1} \dot{\theta}_{2} \cos \left(\theta_{1}-\theta_{2}\right)\\ &+\left(m_{1}+m_{2}\right) g l_{1} \cos \theta_{1}+m_{2} g l_{2} \cos \theta_{2}
\end{aligned}
\end{equation}

With this equation, we can find differential equations for $\theta_{1}$ and $\theta_{2}$, using the Euler-Lagrange Formulation.

Firstly, for $\theta_{1}$, 

\begin{equation}
\frac{\partial L}{\partial \dot{\theta}_{1}}=m_{1} l_{1}^{2} \dot{\theta}_{1}+m_{2} l_{1}^{2} \dot{\theta}_{1}+m_{2} l_{1} l_{2} \dot{\theta}_{2} \cos \left(\theta_{1}-\theta_{2}\right)
\end{equation}

Taking the derivative with respect to time,

\begin{equation}
\begin{aligned}
\frac{d}{d t}\left(\frac{\partial L}{\partial \dot{\theta}_{1}}\right)=&\left(m_{1}+m_{2}\right) l_{1}^{2} \ddot{\theta}_{1}+m_{2} l_{1} l_{2} \ddot{\theta}_{2} \cos \left(\theta_{1}-\theta_{2}\right)\\ & -m_{2} l_{1} l_{2} \dot{\theta}_{2} \sin \left(\theta_{1}-\theta_{2}\right)\left(\dot{\theta}_{1}-\dot{\theta}_{2}\right)
\end{aligned}
\end{equation}

and,

\begin{equation}
\frac{\partial L}{\partial \theta_{2}}=m_{2} l_{1} l_{2} \dot{\theta}_{1} \dot{\theta}_{2} \sin \left(\theta_{1}-\theta_{2}\right)-l_{2} m_{2} g \sin \theta_{2}
\end{equation}

so, the Euler Lagrange differential equation for $\theta_{1}$ becomes:


\begin{equation}
\frac{\partial L}{\partial \theta_{1}}-\frac{d}{d t}\left(\frac{\partial L}{\partial \dot{\theta_{1}}}\right)=0
\end{equation}

Substituting, 

\begin{equation}
\begin{aligned}&\left(m_{1}+m_{2}\right) l_{1}^{2} \ddot{\theta}_{1}+m_{2} l_{1} l_{2} \ddot{\theta}_{2} \cos \left(\theta_{1}-\theta_{2}\right) \\ &+m_{2} l_{1} l_{2} \dot{\theta}_{2}^{2} \sin \left(\theta_{1}-\theta_{2}\right)+l_{1} g\left(m_{1}+m_{2}\right) \sin \theta_{1}=0 \end{aligned}
\label{eq:theta1}
\end{equation}

Similarly, for $\theta_{2}$,

\begin{equation}
\frac{\partial L}{\partial \dot{\theta}_{2}}=m_{2} l_{2}^{2} \dot{\theta}_{2}+m_{2} l_{1} l_{2} \dot{\theta}_{1} \cos \left(\theta_{1}-\theta_{2}\right)
\end{equation}

\begin{equation}
\frac{d}{d t}\left(\frac{\partial L}{\partial \dot{\theta}_{2}}\right)=m_{2} l_{2}^{2} \ddot{\theta}_{2}+m_{2} l_{1} l_{2} \ddot{\theta}_{1} \cos \left(\theta_{1}-\theta_{2}\right)-m_{2} l_{1} l_{2} \dot{\theta}_{1} \sin \left(\theta_{1}-\theta_{2}\right)\left(\dot{\theta}_{1}-\dot{\theta}_{2}\right)
\end{equation}

\begin{equation}
\frac{\partial L}{\partial \theta_{2}}=m_{2} l_{1} l_{2} \dot{\theta}_{1} \dot{\theta}_{2} \sin \left(\theta_{1}-\theta_{2}\right)-l_{2} m_{2} g \sin \theta_{2}
\end{equation}

Thus, the Euler Lagrange differential equation for $\theta_{2}$ becomes:

\begin{equation}
m_{2} l_{2}^{2} \ddot{\theta}_{2}+m_{2} l_{1} l_{2} \ddot{\theta}_{1} \cos \left(\theta_{1}-\theta_{2}\right)-m_{2} l_{1} l_{2} \dot{\theta}_{1}^{2} \sin \left(\theta_{1}-\theta_{2}\right)+l_{2} m_{2} g \sin \theta_{2}=0
\end{equation}
\begin{equation}
m_{2} l_{2} \ddot{\theta}_{2}+m_{2} l_{1} \ddot{\theta}_{1} \cos \left(\theta_{1}-\theta_{2}\right)-m_{2} l_{1} \dot{\theta}_{1}^{2} \sin \left(\theta_{1}-\theta_{2}\right)+m_{2} g \sin \theta_{2}=0
\label{eq:theta2}
\end{equation}

Thus, Equation \ref{eq:theta1} and Equation \ref{eq:theta2} can be used to calculate the acceleration of $\theta_{1}$ and $\theta_{2}$ respectively. 

\begin{thebibliography}{99}

\bibitem{c1} Pendulum, Wikipedia the Free Encyclopedia. Retrieved on October 2, 2019.

\bibitem{c2} Double Pendulum, Wikipedia the Free Encyclopedia. Retrieved on October 2, 2019.

\bibitem{c3} Double Pendulum, from Eric Weisstein's World of Physics. Retrieved on October 2, 2019.

\end{thebibliography}

\end{document}